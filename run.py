from application import create_app
app = create_app('settings')
from settings import HOST, PORT
app.run(host=HOST, port=PORT)
