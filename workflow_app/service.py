import logging
import requests
from settings import HOST, PORT
from flask import json, Response
from application import mongo


def logging_data(data):
    try:
        logging.basicConfig(filename='log.log', level=logging.DEBUG)
        logging.info(data)
    except Exception as e:
        print("Logging exception")
        print("-----", e)


def json_create(data):
    data = json.dumps(data)
    resp = Response(data, status=200, mimetype='application/json')
    return resp


def check_eligibility(eligibility_loan, required):
    if eligibility_loan <= required:
        return {'status': 'requested amount is higher than eligible amount.'}
    else:
        return {'status': 'Congrats', 'eligible': eligibility_loan}


def cbill_fun(data):
    import requests
    url = "http://" + HOST + ":" + PORT + "/workflow/cbill"
    response = requests.post(url, json={"pan_number": data['pan_number']})
    response = response.json()
    logging_data(response)
    cbill = response['cbill']
    if cbill in ["A1", "A2"]:
        workflow.status_update('passed', 'cbill', None, 'processing..')
        logging_data({'status': 'passed', 'response': response})
        return {'status': 'passed', 'response': response, 'reason': 'cbill_passed'}
    else:
        workflow.status_update('failed', 'cbill', 'cbill_failed', 'stopped..')
        logging_data({'status': 'failed', 'reason': 'cbill_failed'})
        return {'status': 'failed', 'response': ['A3 or A4'], 'reason': 'cbill_failed'}


def profile_fun(data):
    url = "http://" + HOST + ":" + PORT + "/workflow/profile"
    response = requests.post(url, json=data)
    response = response.json()
    logging_data(response)
    print(response)
    if response['profile'] == 1:
        workflow.status_update('passed', 'profile', None, 'processing..')
        logging_data({'status': 'passed', 'response': response})
        return {'status': 'passed', 'response': response, 'reason': 'profile_passed'}
    else:
        workflow.status_update('failed', 'profile', 'profile_failed', 'stopped..')
        logging_data({'status': 'failed', 'reason': 'profile_failed', 'response': response})
        return {'status': 'failed', 'response': response['errors'], 'reason': 'profile_failed'}


def policy_fun(data):
    url = "http://" + HOST + ":" + PORT + "/workflow/policy"
    response = requests.post(url, json=data)
    response = response.json()
    logging_data(response)
    if response['resp'] == 'Cleared':
        workflow.status_update('passed', 'policy', None, 'processing..')
        logging_data({'status': 'passed', 'response': response})
        return {'status': 'passed', 'response': response, 'reason': 'policy_passed'}
    else:
        workflow.status_update('failed', 'policy', response['response_data'], 'stopped..')
        logging_data({'status': 'failed', 'reason': 'policy_failed'})
        return {'status': 'failed', 'response': response['errors'], 'reason': 'policy_failed'}


def property_ownershipfun(data):
    try:
        if data['property_ownership'] == 'on':
            return True
    except:
        return False


def new_to_creditfun(data):
    try:
        if data['new_to_credit'] == 'on':
            return True
    except:
        return False

status_id = None


class WorkFlow(object):
    def __init__(self):
        self.status = None
        self.activity = None
        self.reason = None
        self.process = None

    @staticmethod
    def status_update(status=None, activity=None, reason=None, process=None, new=None):
        if new == 'new':
            mongo.db.status.insert({'status': status, 'activity': activity, 'reason': reason, 'process': process})
            status = mongo.db.status.find_one({'activity': activity})
            global status_id
            status_id = status['_id']
        else:
            mongo.db.status.update_one({'_id': status_id}, {"$set": {'_id': status_id, 'status': status,
                                                                     'activity': activity, 'reason': reason,
                                                                     'process': process}}, upsert=True)


workflow = WorkFlow()
