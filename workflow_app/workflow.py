from workflow_app.service import cbill_fun, profile_fun, policy_fun, check_eligibility, logging_data
from celery import Celery


celery_tasks = Celery('service', broker='pyamqp://aravind:aravind@localhost:5672/myvhost', backend='redis://localhost')
switch = {'cbill': cbill_fun, 'profile': profile_fun, 'policy': policy_fun}
list_of_models = ['cbill', 'profile', 'policy', 'disbursement']
eligibility_loan = None


@celery_tasks.task
def workflow_start(data):
    logging_data("Workflow started.")
    from workflow_app.service import WorkFlow
    WorkFlow().status_update('pending', 'started', None, 'starting..', 'new')
    logging_data("DB inserted.")
    d = workflow_move(data)
    return d


def workflow_stop():
    logging_data("Workflow stopped.")


def workflow_move(data):
    global eligibility_loan
    for i in list_of_models:
        logging_data("Calling " + i)
        if i == 'disbursement':
            workflow_stop()
            return check_eligibility(eligibility_loan, data['loan_required'])
        else:
            response = switch[i](data)

            # logging_data(i + " " + response['status'])
            print(response['response'])
            if response['status'] == 'failed':
                workflow_stop()
                return {'status': 'failed', 'errors': response['response']}
            else:
                if i == 'cbill':
                    data.update({'cbill': response['response']['cbill']})
                elif i == 'policy':
                    eligibility_loan = response['response']['amount']
