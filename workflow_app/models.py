from workflow_app.views import api
from flask_restplus import fields


cbill_model = api.model('pan number', {
    'pan_number': fields.String(required=True, description='pan_number')
})

getdbruledata = api.model('version no', {
    'version_no': fields.String(required=True, description='version_no')
})

validate_model = api.model('validate', {
    'first_name': fields.String(required=True, description='First Name'),
    'last_name': fields.String(required=True, description='Last Name'),
    'pan_number': fields.String(required=True, description='property ownership'),
    'qualification': fields.String(required=True, description='qualification'),
    'experience': fields.Integer(required=True, description='experience'),
    'property_ownership': fields.Boolean(required=True, description='property ownership'),
    'new_to_credit': fields.Boolean(required=True, description='New to Credit'),
    'location': fields.String(required=True, description='location'),
    'equipment_type': fields.String(required=True, description='equipment type'),
    'entity_type': fields.String(required=True, description='Esntity Type'),
    'equipment_cost': fields.Integer(required=True, description='equipment type'),
    'down_payment': fields.Integer(required=True, description='equipment type'),
    'loan_required': fields.Integer(required=True, description='loan required'),
    'loan_tenure': fields.Integer(required=True, description='loan tenure (Months)'),
    'insurance_funding': fields.Integer(required=True, description='Insurance funding'),
    'manufacturer': fields.String(required=True, description='Manufacturer'),
    'pep': fields.String(required=True, description='PEP'),
})
