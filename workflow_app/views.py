from flask import Blueprint, jsonify, render_template
from flask_restplus import Api, Resource
from flask_cors import CORS
from json_logic import jsonLogic
from workflow_app.workflow import workflow_start
from application import app, mongo
from workflow_app.service import logging_data, property_ownershipfun, new_to_creditfun
from settings import HOST, PORT


blueprint = Blueprint('work-flow', __name__)
api = Api(blueprint,
          title='PoC',
          version='1.0',
          description='A description',
          doc='/api-doc'
          )


cors = CORS(app)
workflow = api.namespace('workflow', description='work-flow')

from workflow_app.models import validate_model, cbill_model, getdbruledata


@app.route('/')
def home_api():
    return render_template('home.html', resp=None, HOST=HOST, PORT=PORT)


@workflow.route('/getdbruledata')
class getdbruledata_api(Resource):
    @workflow.expect(getdbruledata)
    def post(self):
        data = api.payload
        if data['rule'] == 'Profile':
            resp = mongo.db.profile_rules.find_one({'version': data['version_no']})
        elif data['rule'] == 'Policy':
            resp = mongo.db.policy_rules.find_one({'version': data['version_no']})
        if resp is None:
            return None
        del resp['_id']
        return resp, 200


@app.route('/profile-rule')
def profile_rule_api():
    ver_list = []
    active_version = mongo.db.active.find_one({'version_doc': 'profile'})
    active = active_version['active_version']
    resp = [record for record in mongo.db.profile_rules.find({})]

    for i in resp:
        ver_list.append(i['version'])
    return render_template('profile_rule.html', ver_list=ver_list, HOST=HOST, PORT=PORT, active=active)


@app.route('/policy-rule')
def policy_rule_api():
    ver_list = []
    active_version = mongo.db.active.find_one({'version_doc': 'policy'})
    active = active_version['active_version']
    resp = [record for record in mongo.db.policy_rules.find({})]
    for i in resp:
        ver_list.append(i['version'])
    return render_template('policy_rule.html', ver_list=ver_list, HOST=HOST, PORT=PORT, active=active)


@workflow.route('/update_version')
class UpdateDbVersion(Resource):
    def post(self):
        data = api.payload
        mongo.db.active.update_one({'version_doc': data['for']}, {"$set": {'active_version': data['new_version']}},
                                   upsert=True)
        return {'version': data['new_version']}


@workflow.route('/updatedb')
class UpdateDb(Resource):
    def post(self):
        data = api.payload
        version = data['version']
        if data['rule'] == 'profile':
            mongo.db.profile_rules.update_one({'version': version}, {"$set": {'rules': data['update_data']}},
                                              upsert=True)
        elif data['rule'] == 'policy':
            route = "rules." + data['amount']
            mongo.db.policy_rules.update_one({'version': version}, {"$set": {route: data['update_data']}}, upsert=True)
        return {'status': 'success'}


@workflow.route('/cbill')
class Cbill(Resource):
    @workflow.expect(cbill_model)
    def post(self):
        data = api.payload
        pan_number = int(data['pan_number'])
        rules1 = {"<=": [{"var": "temp"}, 550]}
        rules2 = {"and": [
            {"<=": [551, {"var": "temp"}]},
            {"<=": [{"var": "temp"}, 650]}
        ]}
        rules3 = {"and": [
            {"<=": [651, {"var": "temp"}]},
            {"<=": [{"var": "temp"}, 706]}
        ]}
        request = {"temp": pan_number}
        if jsonLogic(rules1, request):
            return jsonify({'cbill': "A3"})
        elif jsonLogic(rules2, request):
            return jsonify({'cbill': "A2"})
        elif jsonLogic(rules3, request):
            return jsonify({'cbill': "A1"})


@workflow.route('/profile')
class Profile(Resource):
    @workflow.expect()
    def post(self):
        data = api.payload
        data.update({'loan_required': int(data['loan_required'])})
        data.update({'ltv': int(data['ltv'])})
        data.update({'insurance_funding': int(data['insurance_funding'])})
        data.update({'loan_tenure': int(data['loan_tenure'])})
        rules = []
        errors = []
        active_version = mongo.db.active.find_one({'version_doc': 'profile'})
        rule_doc = mongo.db.profile_rules.find_one({'version': active_version['active_version']})
        for i in rule_doc['rules']:
            rules.append(i)
        for j in rules:
            rule = rule_doc['rules'][j]
            cond = rule['cond']
            rules1 = {cond: [{"var": "val"}, {"var": "db_rule.value"}]}
            rules2 = {cond: [{"var": "db_rule.start"}, {"var": "val"}, {"var": "db_rule.end"}]}
            request = {"val": data[j], "db_rule": rule}
            if j in ["qualification", "manufacturer", "pep", "new_to_credit", "location", "cbill"]:
                if j in ["new_to_credit"]:
                    if rule['value'] is True:
                        pass
                    else:
                        if not jsonLogic(rules1, request):
                            errors.append(j + " - Error")
                elif not jsonLogic(rules1, request):
                    errors.append(j + " - Error")
            elif j in ["loan_required", "loan_tenure", "ltv", "insurance_funding"]:
                if not jsonLogic(rules2, request):
                    errors.append(j + " - Error")
        if errors:
            return jsonify({'profile': False, 'errors': errors, 'resp': 'Failed'})
        else:
            return jsonify({'profile': True, 'errors': None, 'resp': 'Cleared'})


@workflow.route('/policy')
class Policy(Resource):
    @workflow.expect()
    def post(self):
        from workflow_app.service import json_create
        data = api.payload
        data.update({'experience': int(data['experience'])})
        final_eligible_amounts = []
        eligible_amounts = []
        active_version = mongo.db.active.find_one({'version_doc': 'policy'})
        rule_doc = mongo.db.policy_rules.find_one({'version': active_version['active_version']})
        for i in rule_doc['rules']:
            eligible_amounts.append(i)
        eligible_amounts = [int(item) for item in eligible_amounts]
        eligible_amounts.sort(reverse=True)
        eligible_amounts = [str(item) for item in eligible_amounts]
        for j in eligible_amounts:
            eligible_rule = rule_doc['rules'][j]
            rules = []
            errors = []
            for i in eligible_rule:
                rules.append(i)
            for k in rules:
                rule = eligible_rule[k]
                cond = rule['cond']
                rules1 = {cond: [{"var": "val"}, {"var": "db_rule.value"}]}
                rules2 = {cond: [{"var": "db_rule.start"}, {"var": "val"}, {"var": "db_rule.end"}]}
                request = {"val": data[k], "db_rule": rule}
                if k in ["qualification", "equipment_type", "entity_type"]:
                    if not jsonLogic(rules1, request):
                        errors.append(k + " - Error")
                elif k in ["property_ownership", "experience"]:
                    if k in ["experience"]:
                        if cond == '<=':
                            if not jsonLogic(rules2, request):
                                errors.append(k + " - Error")
                        else:
                            if not jsonLogic(rules1, request):
                                errors.append(k + " - Error")
                    else:
                        if not jsonLogic(rules1, request):
                            errors.append(k + " - Error")
            if errors:
                pass
            else:
                final_eligible_amounts.append(j)

        if final_eligible_amounts:
            final_eligible_amounts = [int(item) for item in final_eligible_amounts]
            final_eligible_amount = max(final_eligible_amounts)
            return json_create({'amount': final_eligible_amount, 'resp': 'Cleared', 'response_data': None})
        else:
            return json_create({'resp': 'failed', 'response_data': "Doesn't meet the experience criteria"})


@workflow.route('/workflow')
class WorkflowAPI(Resource):
    @workflow.expect(validate_model)
    def post(self):
        data = api.payload
        data.update({'experience': int(data['experience']),
                     'property_ownership': property_ownershipfun(data),
                     'new_to_credit': new_to_creditfun(data),
                     'equipment_cost': int(data['equipment_cost']),
                     'down_payment': int(data['down_payment']),
                     'loan_required': int(data['loan_required']),
                     'loan_tenure': int(data['loan_tenure']),
                     'insurance_funding': int(data['insurance_funding']),
                     })
        logging_data(data)
        data.update({'ltv': (data['loan_required'] / data['equipment_cost']) * 100})
        resp = workflow_start.apply_async(args=[data])
        while not workflow_start.AsyncResult(resp.id).status == "SUCCESS":
            pass
        resp = resp.result
        return resp, 200
