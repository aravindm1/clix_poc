from flask import Flask
from flask_pymongo import PyMongo
from flask_bcrypt import Bcrypt


def create_app(config_filename):
    global app
    app = Flask(__name__)
    app.config.from_object(config_filename)
    global mongo
    global bcrypt
    mongo = PyMongo(app)
    bcrypt = Bcrypt(app)
    from workflow_app.views import blueprint
    app.register_blueprint(blueprint)
    return app


def mongo_app(config_filename):
    global app
    app = Flask(__name__)
    app.config.from_object(config_filename)
    global mongo
    mongo = PyMongo(app)
    return app


app = mongo_app('settings')
mongo = PyMongo(app)
bcrypt = Bcrypt(app)